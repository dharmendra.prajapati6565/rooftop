<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Coach;

/**
 * Site controller
 */
class CoachController extends Controller
{
     public function actionAvailability() {

        $coachModel = new Coach();

        $coaches = $coachModel->find()->all();

        return $this->render( 'availability', [ 'coaches' => $coaches ] );
     }

     public function actionView( $id ) {
         $coachModel = new Coach();
         $coach = $coachModel->find()->where( " id = $id " )->one();
         return $this->render( 'view', [ 'coach' => $coach ] );
     }

     public function actionBook( $coach, $time ) {
      $coachModel = new Coach();
      $coach = $coachModel->find()->where( " id = $coach " )->one();

      return $this->render( 'book', [ 'coach' => $coach, 'time' => $time ] );
     }

     public function actionConfirm( $coach, $time ) {

      return $this->render( 'confirm', [ 'coach' => $coach, 'time' => $time ] );
     }
}
