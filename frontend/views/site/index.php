<?php

/* @var $this yii\web\View */ 
use yii\helpers\Url;

$this->title = 'Rooftop Coding Challenge';

$siteUrl = Url::base();
?>
<div class="site-index">
    <div class="mt-5 text text-center">
        <a href="<?= $siteUrl.'/coach/availability'; ?>" class="btn btn-success">View Available Coaches</a>    
    </div>
</div>
