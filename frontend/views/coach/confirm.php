<?php

/* @var $this yii\web\View */ 
use yii\helpers\Url;

$this->title = 'Confirm - Rooftop Coding Challenge';
?>

<div class="confirm text text-center">
    <h1 class="text text-white">Congratulations!</h1> <hr>
    <h5 class='text text-white mt-4'>
        Your appointment is successfully sheduled with <br> <b><?=$coach?></b> <br>
        <br>
        at <b><?=$time?></b>
    <h5>    
</div>    