<?php

/* @var $this yii\web\View */ 
use yii\helpers\Url;

$this->title = 'Coaches Availability - Rooftop Coding Challenge';

$siteUrl = Url::base();

$today =  date('l');
?>

<div class="wrap-coaches p-5 text text-center">
    <h1>Please choose available coaches</h1>
    <ul class="coaches">
        <?php foreach( $coaches as $coach ) { 
            
            if( $today == $coach->day_of_week ) {
            
            ?>
            <li>
                <span class="coach-img">
                    <img src="https://i.picsum.photos/id/6/200/200.jpg?hmac=g4Q9Vcu5Ohm8Rwap3b6HSIxUfIALZ6BasESHhw7VjLE">
                </span>    
                <h5 class="mt-3"><?=$coach->name;?></h5>
                <a href="<?=$siteUrl.'/coach/view?id='.$coach->id?>" class="btn btn-dark btn-select">Select</a>
            </li>    
        <?php } } ?>    
    </ul>    
</div>    