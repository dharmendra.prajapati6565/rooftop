<?php

/* @var $this yii\web\View */ 
use yii\helpers\Url;

$this->title = 'Book - Rooftop Coding Challenge';

$siteUrl = Url::base();

$today =  date('l');
$coachId = $coach->id;
$coachName = $coach->name;
?>

<div class="wrap-details row">
    <div class="col-md-4">
        <h5>Coach-<?=$coach->name?></h5>
        <span class="coach-img coach-img-2">
            <img src="https://i.picsum.photos/id/6/200/200.jpg?hmac=g4Q9Vcu5Ohm8Rwap3b6HSIxUfIALZ6BasESHhw7VjLE">
        </span> 
        <br>
        <h4> Meeting time - <b><?=$time?></b>
        <h4> Duration - <b>30 Minutes</b>
    </div>    
    <div class="col-md-8 wrap-slots">
        <h5>Please fill up your details below</h5>
        <form>
            <input class="form-control" type="text" placeholder="Enter Name">
            <input class="form-control mt-3" type="email" placeholder="Enter Email">
            <textarea class="form-control mt-3" placeholder="Reason for scheduling this meeting"></textarea>
            <a class="btn btn-success mt-3" href="<?=$siteUrl?>/coach/confirm?coach=<?=$coachName?>&time=<?=$time?>">Schedule</a>
        </form>    
    </div>
</div> 