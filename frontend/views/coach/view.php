<?php

/* @var $this yii\web\View */ 
use yii\helpers\Url;

$this->title = $coach->name.' - Rooftop Coding Challenge';

$siteUrl = Url::base();

$today =  date('l');
$coachId = $coach->id;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript">

jQuery( document ).ready( function() {
        
    let x = {
    slotInterval: 30,
    openTime: '<?=$coach->available_at?>',
    closeTime: '<?=$coach->available_until?>'
    };

    //Format the time
    let startTime = moment(x.openTime, "HH:mm");

    //Format the end time and the next day to it 
    let endTime = moment(x.closeTime, "HH:mm");

    //Times
    let allTimes = [];

    //Loop over the times - only pushes time with 30 minutes interval
    while (startTime < endTime) {
    //Push times
    allTimes.push(startTime.format("HH:mm")); 
    //Add interval of 30 minutes
    startTime.add(x.slotInterval, 'minutes');
    }

    var timeSlots = '';
    //console.log(allTimes);

    var i;
    for (i = 0; i < allTimes.length; ++i) {
    
        timeSlots += "<li>";
            timeSlots += "<a href='<?=$siteUrl?>/coach/book?coach=<?=$coachId?>&time="+allTimes[i]+"' class='btn btn-info' href='#'>"+allTimes[i]+"</a>";
        timeSlots += "</li>";
    }
    

    jQuery( ".wrap-coach-slots" ).html(timeSlots);
} );
</script>    
<div class="wrap-details row">
    <div class="col-md-4">
        <h5>Coach-<?=$coach->name?></h5>
        <span class="coach-img coach-img-2">
            <img src="https://i.picsum.photos/id/6/200/200.jpg?hmac=g4Q9Vcu5Ohm8Rwap3b6HSIxUfIALZ6BasESHhw7VjLE">
        </span> 
    </div>    
    <div class="col-md-8 wrap-slots">
        <h5>Available Slots for - <b><?=$today?></b></h5>
        <ul class="wrap-coach-slots">

        </ul>    
    </div>
</div>    